<!DOCTYPE html>
<html>
<head>
	<title>Add Reservation</title>
	<style type="text/css">
		.form-group{
			padding: 5px;
		}
	</style>
</head>
<?php
	$con = mysqli_connect("localhost", "root", "", "cs306-sailorsdb")
				or die("Databse error!");
	$boats = mysqli_query($con, "SELECT * FROM boats");
	$sailors = mysqli_query($con, "SELECT * FROM sailors");
?>
<body>
<form method="POST" action="postreservationsform.php">
	<fieldset>
		<legend>Add Reservations</legend>
		<div class="form-group">
			<label>Select boat</label>
			<select name="bid">
				<?php 
					foreach ($boats as $key => $value) {	
						echo "<option value='". $value["bid"]. "'>". $value["bname"]. "</option>";		
					} 
				?>
			</select>
		</div>
		<div class="form-group">
			<label>Select sailor</label>
			<select name="sid">
				<?php 
					foreach ($sailors as $key => $value) {	
						echo "<option value='". $value["sid"]. "'>". $value["sname"]. " Rating( ". $value['rating'] ." ) Age( ". $value['age'] ." )</option>";		
					} 
				?>
			</select>
		</div>
		<div class="form-group">
			<input type="date" name="rdate">
		</div>
		<div class="form-group">
			<input type="submit" name="submit" value="Add Reservation">
		</div>
	</fieldset>
</form>
</body>
</html>
