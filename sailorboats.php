<?php 

// the mysql connection

$con = mysqli_connect('localhost', 'root', '', 'cs306-sailorsdb');

$sql = mysqli_query($con, 'CREATE VIEW IF NOT EXISTS SailorBoatsView AS SELECT sailors.sid AS sid, sname, boats.bid AS bid, bname, COUNT(*) AS "no_of_reservations" FROM reserves INNER JOIN boats ON reserves.bid=boats.bid INNER JOIN sailors ON reserves.sid=sailors.sid GROUP BY boats.bid');
// $res = mysqli_result($sql);

$sql = mysqli_query($con, "SELECT * FROM SailorBoatsView");


$str = "<table border='1'>";
$str .= "<tr>";
$str .= "<th>Sailor ID</th>";
$str .= "<th>Sailor Name</th>";
$str .= "<th>Boat ID</th>";
$str .= "<th>Boat Name</th>";
$str .= "<th>No of Reservations</th>";
$str .= "</tr>";

while($row = mysqli_fetch_object($sql)) {
	$str .= "<tr>";
	$str .= "<td>$row->sid</td>";
	$str .= "<td>$row->sname</td>";
	$str .= "<td>$row->bid</td>";
	$str .= "<td>$row->bname</td>";
	$str .= "<td>$row->no_of_reservations</td>";
	$str .= "</tr>";
}
$str .= "</table>";

echo $str;
?>